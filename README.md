# Candy - The PHP functional MVT framework

If you need a simple lightweight solution that just works out of the box without all those bloated dependencies and still does everything you need, or you just need a solution that does what those giant frameworks do with a light footprint, or you are new to PHP developments and want to use PHP the simpler way, then Candy is for you

Candy has everything you need to develop a full functional professional website and scale along.

### Features

- Light weight
- Inbuilt development server
- Easily deploy to shared a server
- Chocolate - A simple blade-like advanced template engine with device specific override ability.
- Simple and flexible model based system
- Routing
- Flexible form and validation API
- Simple Database API
- Extensible hashtag based configuration
- File based view system
- Automatic language translation system
- File processing
- Plug-in and hook system
- Simple Session, Post, Get, Put
- File upload management
- Cookie management
- Multiple database support via PDO
- Advanced regular expression library
- Device identification with support for custom UI's for popular devices
- HTTP management
- Image manipulation
- Automatic detection and management

### Installation

```
git clone https://github.com/mcfriend99/Candy.git
```

### Starting the server

```
php pop server
```

### Activating maintenance mode

```
php pop hide
```

### Deactivating maintenance mode

```
php pop lick
```

```markdown

1. Numbered
2. List

**Bold** and _Italic_ and `Code` text

[Link](url) and ![Image](src)
```
